﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLIntegration.SQLException
{
    public class InvalidResultException : Exception
    {
        public InvalidResultException()
            : base()
        {
        }
    }
}

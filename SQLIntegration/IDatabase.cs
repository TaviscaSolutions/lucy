﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lucy.Handlers.SQLIntegration
{
    public interface IDatabase
    {

        DataTable GetResult(SqlCommand query, Dictionary<string, object> dictionary); 

    }
}

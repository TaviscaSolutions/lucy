﻿using Lucy.Core.Contracts;
using Lucy.Core.Model;
using SQLIntegration.SQLException;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace Lucy.Handlers.SQLIntegration
{
    public class SQLCommandHandler : ISQLCommandHandler, ICanRecieveMessage
    {
        public const string QUERY_NAME = "query_name";
        public const string SP_NAME = "sp_name";
        public IDatabase Database { get; private set; }
        QueryDataSource _querySource { get; set; }

        class QueryDataSource
        {
            public SqlCommand GetQuery(string query)
            {
                if (string.IsNullOrEmpty(query))
                {
                    throw new ArgumentNullException();
                }
                else
                {
                    SqlCommand sqlQuery = new SqlCommand(query);
                    return sqlQuery;
                }
            }
        }

        public XmlDocument Execute(Command command)
        {
            if (string.IsNullOrEmpty(command.Type))
            {
                throw new InvalidCommandTypeException();
            }
            else
            {
                //Instantiate QueryDataSource class
                _querySource = new QueryDataSource();

                //Extract command arguments

                SqlCommand query = null;
                if(command.Arguments.ContainsKey(QUERY_NAME))
                query = this._querySource.GetQuery(command.Arguments[QUERY_NAME]);
                else if (command.Arguments.ContainsKey(SP_NAME))
                    query = this._querySource.GetQuery(command.Arguments[SP_NAME]);
                //Map Stored Procedure Name

                //Instantiate SqlDatabase class
                Database = new SqlDatabase();

                //Create SQLDatabase object and call convert method
                var result = this.Database.GetResult(query, GetArguments(command.Arguments));
                                
                //Create XMLConverter objce and call convert method
                return new TableAdapter().ConvertToXml(result);
            }
        }

        private Dictionary<string, object> GetArguments(Dictionary<string, string> dictionary)
        {
            Dictionary<string, object> argumentDictionary = new Dictionary<string, object>();
            foreach (KeyValuePair<string, string> argument in dictionary)
            {
                if(argument.Key != QUERY_NAME)
                argumentDictionary[argument.Key] = (argument.Value as Object);
            }
            return argumentDictionary;
        }

        public void Notify(Message message)
        {
            Command command = message.Payload as Command;
            Execute(command);
        }

        public string Id
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}

﻿using SQLIntegration.SQLException;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;

namespace Lucy.Handlers.SQLIntegration
{
    public class SqlDatabase : IDatabase
    {
        private SqlConnection _sqlConnection { get; set; }

        public string ConnectionString { get; set; }

        static private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString;
        }

        public SqlDatabase()
        {
            this.ConnectionString = GetConnectionString();
            if (this.ConnectionString != "")
            {
                if (this.ConnectionString == null)
                {
                    throw new DatabaseConnectionException("Connection string cannot be null.");
                }
            }
            else
            {
                throw new DatabaseConnectionException("Connection string cannot be empty.");
            }
        }

        //Call specified stored procedure and get the result
        public DataTable GetResult(SqlCommand command, Dictionary<string, object> dictionary)
        {
            SqlDataAdapter adapter;
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            _sqlConnection = new SqlConnection(this.ConnectionString);
            try
            {
                _sqlConnection.Open();
            }
            catch (Exception ex)
            {

            }

            command.Connection = _sqlConnection;

            if (dictionary.ContainsKey("query_name"))
            {
                command.CommandText = dictionary["query_name"] as String;
                command.CommandType = CommandType.Text;
            }

            else if (dictionary.ContainsKey("sp_name"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = dictionary["sp_name"] as String;                
                command.Parameters.AddRange(CreateInputSqlParameterArray(dictionary));
            }
            
            adapter = new SqlDataAdapter(command);
            adapter.Fill(dt);

            _sqlConnection.Close();
            return dt;
        }

        //Method for creating input parameters of stored procedure
        private SqlParameter[] CreateInputSqlParameterArray(Dictionary<string, object> dictionary)
        {
            SqlParameter[] inputParameters = new SqlParameter[dictionary.Count - 1];
            int paramIndex = 0;
            foreach(KeyValuePair<string, object> parameter in dictionary)
            {
                if (parameter.Key != "query_name" && parameter.Key != "sp_name")
                {
                    SqlParameter sqlParam = new SqlParameter();
                    sqlParam = new SqlParameter("@Game_id", parameter.Value);
                    sqlParam.Direction = ParameterDirection.Input;
                    sqlParam.DbType = DbType.Int32;
                    inputParameters[paramIndex] = sqlParam;   
                    paramIndex++;
                }            
            }
            return inputParameters;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Data;
using System.Xml;
using Lucy.Core.Model;

namespace Lucy.Handlers.SQLIntegration
{
    public interface ISQLCommandHandler
    {
        //method to be called by dispatcher for executing the query
        XmlDocument Execute(Command command);
    }
}

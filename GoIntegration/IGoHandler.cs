﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Lucy.Core;
using Lucy.Core.Model;

namespace GoIntegration
{
    public interface IGoHandler
    {
        // To be called by Dispatcher
        XmlDocument ExecuteGo(Command command);
    }
}

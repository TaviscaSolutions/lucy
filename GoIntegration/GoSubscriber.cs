﻿using Lucy.Core.Contracts;
using Lucy.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GoIntegration
{
    class GoSubscriber : ICanRecieveMessage
    {
        public void Notify(Message message)
        {
            Command command = (Command)message.Payload;
            XmlDocument xmlDoc;
            GoHandler goHandler=new GoHandler();
            xmlDoc=goHandler.ExecuteGo(command);
            TopicMessage messageToBeReturned = new TopicMessage(this, "Formatter", (object)xmlDoc);            
        }


        string ICanRecieveMessage.Id
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        void ICanRecieveMessage.Notify(Message message)
        {
            throw new NotImplementedException();
        }
    }
}

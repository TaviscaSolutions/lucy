﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GoIntegration
{
    class GoHandler : IGoHandler
    {
        public System.Xml.XmlDocument ExecuteGo(Lucy.Core.Model.Command command)
        {
            GoIntegrator goIntegrator = new GoIntegrator();
            int len= command.Arguments.Count();
            XmlDocument xmlDoc = new XmlDocument();
            if (command.Type == "GoWithLatest")
            {
                xmlDoc = goIntegrator.TriggerPipelineInstance(command.Arguments["pipeline_name"]);
            }
            if (command.Type == "GoWithCommitId")
            {
                xmlDoc = goIntegrator.TriggerPipelineInstanceWithCommitId(command.Arguments["pipeline_name"], command.Arguments["commitId"]);
            }
            return xmlDoc;
        }
    }
}

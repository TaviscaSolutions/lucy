﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace GoIntegration
{
    public interface IGoIntegrator
    {

        XmlDocument TriggerPipelineInstance(string pipelineName);

        XmlDocument TriggerPipelineInstanceWithCommitId(string pipelineName,string commitTag);

    }
}

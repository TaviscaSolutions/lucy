﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lucy.Core.Model;
using Lucy.Core.Contracts;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Xml.Linq;
using Lucy.Core.CustomAttributes;

namespace GoIntegration
{
    public class GoIntegrator : IGoIntegrator
    {

        private const string URL = "http://hfl-build.pune.tavisca.com:8153/go/api/pipelines/";
        private const string DATA = @"{""object"":{""name"":""Name""}}";

        [CommandArgumentAttribute("Pipeline Name", "string", "Name of the pipeline to be run.")]
        [CommandAttribute("TiggerPipelineInstance(string [pipeline_name])", "Runs the pipeline and returns the status of execution")]
        [CommandReturnsAttribute("XmlDocument", "Returns the status of pipeline")]
        public XmlDocument TriggerPipelineInstance(string pipelineName)
        {
              string url=URL+pipelineName+"/schedule";
              //string  url = URL +pipelineName+ "hfl-local/schedule";
              string response=_GetResponseFromGo(url);
              string xmlResponse = "<status>" + response + "</status>";
              XmlDocument xmlDoc = new XmlDocument();
              xmlDoc.LoadXml(xmlResponse);
              Console.WriteLine(xmlResponse);
              return xmlDoc;
        }

        [CommandArgumentAttribute("PipelineName,CommitId", "string,string", "Name of the pipeline to be run,Revision no of pipeline to be run.")]
        [CommandAttribute("TriggerPipelineInstance(string [pipeline_name])", "Runs the pipeline and returns the status of execution")]
        [CommandReturnsAttribute("XmlDocument", "Returns the status of pipeline")]
        public XmlDocument TriggerPipelineInstanceWithCommitId(string pipelineName, string commitTag)
        {
            string url = "materials[svn_material]="+ commitTag + " " + URL + pipelineName+ "/schedule";
            string response = _GetResponseFromGo(url);
            string xmlResponse = "<status>" + response + "</status>";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlResponse);
            Console.WriteLine(xmlResponse);
            return xmlDoc;
        }

        private string _GetResponseFromGo(string url)
        {
            string response = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = DATA.Length;
            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(DATA);
            requestWriter.Close();

            try
            {
                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                response = responseReader.ReadToEnd();
                Console.Out.WriteLine(response);
                responseReader.Close();
            }
            catch (Exception e)
            {
                GoCustomExceptions goServerNotFound = new GoCustomExceptions("GoServerNotFound", "Connection to go server not established " + e.Message);
                throw goServerNotFound;
            }
            return response;

        }

        private System.Xml.XmlDocument _ConvertToResponse(System.Xml.XmlDocument inputDocument)
        {
            XDocument doc = XDocument.Load(@"D:\ConsoleApplication3\ConsoleApplication3\Sample.xml");
            var entries = doc.Descendants("entry")
                                .Select(x => new
                                {
                                    Title = (string)x.Element("title"),
                                    Id = (string)x.Element("id")
                                });

            foreach (var entry in entries)
            {
                Console.WriteLine("Start: {0}; End: {1}", entry.Title, entry.Id);
            }
            Console.ReadLine();

            XmlDocument xmlDoc1 = new XmlDocument();
            XmlNode rootNode = xmlDoc1.CreateElement("GoResponse");
            XmlNode feedNode = xmlDoc1.CreateElement("feed");

            xmlDoc1.AppendChild(rootNode);
            rootNode.AppendChild(feedNode);


            XmlNode entryNode;
            XmlNode titleNode;
            XmlNode idNode;
            foreach (var project in entries)
            {
                entryNode = xmlDoc1.CreateElement("entry");
                titleNode = xmlDoc1.CreateElement("title");
                idNode = xmlDoc1.CreateElement("id");

                //                Console.WriteLine("Start: {0}; End: {1}", project.Title, project.Id);

                titleNode.InnerXml = project.Title;
                idNode.InnerXml = project.Id;

                feedNode.AppendChild(entryNode);
                entryNode.AppendChild(titleNode);
                entryNode.AppendChild(idNode);
            }
            return xmlDoc1;
            //xmlDoc1.Save("Samples.xml");            
        }
        
        public string RequestResponse(string url)
        {
            string response=string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = DATA.Length;
            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(DATA);
            requestWriter.Close();

            try
            {
                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                response = responseReader.ReadToEnd();
                Console.Out.WriteLine(response);
                responseReader.Close();
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("-----------------");
                Console.Out.WriteLine(e.Message);
            }
            return response;

            }


    }

}



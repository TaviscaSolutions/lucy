﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GoIntegration
{
    public class GoCustomExceptions : Exception
    {
        public string Type { get; set; }
        public string Description { get; set; }

        public GoCustomExceptions(string type, string desc)
        {
            this.Type = type;
            this.Description = desc;
        }
            
    }
}

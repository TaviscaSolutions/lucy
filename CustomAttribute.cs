﻿using System;
using System.Globalization;
using System.ComponentModel.DataAnnotations;

[AttributeUsage(AttributeTargets.Property |
  AttributeTargets.Field, AllowMultiple = false)]
sealed public class CustomAttributes : ValidationAttribute
{

    public override bool IsValid(object value)
    {
        bool result = true;
        if (value == null)
            result = true;
        else
            result = false;
        return result;
    }

    public override string FormatErrorMessage(string name)
    {
        return String.Format(CultureInfo.CurrentCulture,
          ErrorMessageString, name);
    }
}

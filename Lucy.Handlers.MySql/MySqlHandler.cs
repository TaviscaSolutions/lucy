﻿using Lucy.Core.Contracts;
using Lucy.Core.Model;
using Lucy.Handlers.MySql.CustomException;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Lucy.Infrastructure;

//This needs to be done int Init file
//This will register MySqlHandler with MEF
/* _dependencyContainerObject = new DependencyContainer();
 var SqlHandler = _dependencyContainerObject.Register<ICanRecieveMessage>("Lucy.Handlers.MySql.MySqlHandler,Lucy.Handlers.MySql");
 */
namespace Lucy.Handlers.MySql
{
    public class MySqlHandler : ICanRecieveMessage
    {
        IDatabase Database = null;
        private QueryDataSource _querySource = new QueryDataSource();
        DependencyContainer _dependencyContainerObject = null;

        public string Id
        {
            set;
            get;
        }
        public MySqlHandler(string connectionString)
        {
            Id = "MySQL";
            Database = new MySqlDatabase(connectionString);
            //IChannel dispatcher = (IChannel)_dependencyContainerObject.Create(typeof(IChannel), "Lucy.Core.Contracts");
            Dispatcher dispatcher = new Dispatcher();
            dispatcher.Subscribe("MySql", this);
        }

        public void Notify(Message message)
        {
            Command command = (Command)message.Payload;
            XmlDocument xmlDocument = Execute(command);
            message.Reply(this, (object)xmlDocument);
        }

        
        public XmlDocument Execute(Command command)
        {
            string[] parameterList = null;
            try
            {
                if (command.Arguments.ContainsKey("sp_name"))
                {
                    parameterList = this._querySource.GetParameterList(command.Arguments);
                    if (parameterList.Length != command.Arguments.Count - 1)
                        throw new InvalidParameterException();
                }
                return new TableAdapter()
                    .ToXml(this.Database
                            .ExecuteCommand(this.Database
                                .CreateCommand(command.Arguments, parameterList)));
            }
            catch (InvalidParameterException e)
            {
                string xmlData = "<Error>Invalid Parameters</Error>";
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlData);
                return xmlDoc;
            }
            catch (ProcedureNotFoundException e)
            {
                string xmlData = "<Error>No such procedure found</Error>";
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlData);
                return xmlDoc;
            }
            catch (Exception e)
            {
                string xmlData = "<Error>Request cannot be processed</Error>";
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlData);
                return xmlDoc;
            }
        }
    }
}

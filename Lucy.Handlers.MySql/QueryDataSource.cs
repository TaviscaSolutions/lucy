﻿using Lucy.Handlers.MySql.CustomException;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Lucy.Handlers.MySql
{
    public class QueryDataSource
    {
        private Dictionary<string, string> _commandMapper = new Dictionary<string, string>();

        public QueryDataSource()
        {
            InitialiseCommandMapper();
        }

        public string[] GetParameterList(Dictionary<string, string> dictionary)
        {
            if (_commandMapper.ContainsKey(dictionary["sp_name"]))
                return _commandMapper[dictionary["sp_name"]].Split(',');
            else
                throw new ProcedureNotFoundException();
        }

        private void InitialiseCommandMapper()
        {
            string _configPath = Path.Combine(Environment.CurrentDirectory, "CommandMapper.xml");
            try
            {
                XmlReader reader = XmlReader.Create(_configPath);
                string commandName = null;
                string parameters = null;
                while (reader.Read())
                {

                    if (reader.Name.ToString().Equals("CommandName"))
                        commandName = reader.ReadString();
                    if (reader.Name.ToString().Equals("Parameters"))
                        parameters = reader.ReadString();
                    if (commandName != null && parameters != null)
                    {
                        _commandMapper.Add(commandName, parameters);
                        commandName = null;
                        parameters = null;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }


    }
}

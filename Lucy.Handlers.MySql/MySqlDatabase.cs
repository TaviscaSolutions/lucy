﻿using Lucy.Handlers.MySql.CustomException;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lucy.Handlers.MySql
{
    public class MySqlDatabase : IDatabase
    {
        public MySqlDatabase(string connString)
        {
            if (connString == null || connString == string.Empty)
                throw new InvalidConnectionStringException();
            this.ConnectionString = connString;
        }

        public string ConnectionString { get; set; }
        
        public DataTable ExecuteCommand(MySqlCommand command)
        {
            using (var conn = new MySqlConnection(this.ConnectionString))
            {
                conn.Open();
                command.Connection = conn;
                DataTable table = new DataTable();
                table.Load(command.ExecuteReader());
                return table;
            }

        }

        public MySqlCommand CreateCommand(Dictionary<string, string> dictionary,string[] parameterList)
        {
            MySqlCommand cmd = new MySqlCommand();
            if (dictionary.ContainsKey("query_name"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = dictionary["query_name"];
            }
            else if (dictionary.ContainsKey("sp_name"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = dictionary["sp_name"];
                
                for (int i = 0; i < parameterList.Length; i++)
                {
                    string parameterName = "@" + parameterList[i];
                    object parameterValue = dictionary[parameterList[i]];
                    if (parameterValue == null)
                        throw new InvalidParameterException();
                    cmd.Parameters.AddWithValue(parameterName, parameterValue);
                    cmd.Parameters[parameterName].Direction = ParameterDirection.Input;
                }
            }
            return cmd;
        }
       

    }
}

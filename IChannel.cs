﻿using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;
using System;

[MetadataType(typeof(IChannelMetadata))]
public partial class IChannel
{
  [CustomAttribute(parm1,
  ErrorMessage = "{0} field validation failed.")]
    public object Phone;
}

public class IChannelMetadata
{

}
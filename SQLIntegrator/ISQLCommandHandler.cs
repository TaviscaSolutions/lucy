﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;
using System.Xml;
using Lucy.Core.Model;

namespace SQLIntegrator
{
    [ServiceContract]
    public interface ISQLCommandHandler
    {
        //method to be called by dispatcher for executing the query
        [OperationContract]
        XmlDocument ExecuteSP(Command command);
    }
}

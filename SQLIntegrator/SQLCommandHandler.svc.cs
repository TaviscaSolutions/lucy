﻿using Lucy.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;

namespace SQLIntegrator
{
    public class SQLCommandHandler : ISQLCommandHandler
    {
        //Maps the stored procedure name and returns actual SP name
        private string GetSPName(string commandType)
        {
            throw new NotImplementedException();
        }

        //Load the stored procedure
        private bool LoadSP(string sPName)
        {
            throw new NotImplementedException();
        }

        //Extract the parametr attribute from command object for attaching it with the SP call
        private string GetSPParameters(List<string> commandParameters)
        {
            throw new NotImplementedException();
        }

        //Create a connection with the database
        private bool CreateConnection(string details)
        {
            throw new NotImplementedException();
        }

        //Convert result obtained from database into XML
        private XmlDocument ConvertResultIntoXML(DataTable result)
        {
            throw new NotImplementedException();
        }

        //return converted result to the channel 
        private bool ReturnResultToChannel(string result)
        {
            throw new NotImplementedException();
        }

        //Overriden function comprising of implementation of command handler
        public XmlDocument ExecuteSP(Command command)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System.Collections.Generic;
using System.Xml;

namespace Help
{
    public class HelpXmlManager
    {
        public void AddCommandToXmlDocument(XmlDocument xmlDocument, string commandName, List<string> commandParameteres)
        {
            if (xmlDocument == null) return;

            var commandElements = xmlDocument.GetElementsByTagName("Commands");

            if (commandElements.Count != 1) return;

            var commandElement = xmlDocument.CreateElement("Command");
            var commandsNode = xmlDocument.GetElementsByTagName("Commands").Item(0);
            commandsNode.AppendChild(commandElement);

            commandParameteres.ForEach(x => AppendArgumentElementToCommandNode(x, commandElement, xmlDocument));
        }

        private static void AppendArgumentElementToCommandNode(string attributeName, XmlElement commandElement, XmlDocument xmlDocument)
        {
            var commandArgumentElement = xmlDocument.CreateElement("CommandArgument");
            commandArgumentElement.SetAttribute("Name", attributeName);
            commandElement.AppendChild(commandArgumentElement);
        }

        public XmlDocument GetDefaultXmlDocument()
        {
            var xmlDocument = new XmlDocument();

            var rootNode = xmlDocument.CreateElement("Help");

            xmlDocument.AppendChild(rootNode);

            var commandsNode = xmlDocument.CreateElement("Commands");

            rootNode.AppendChild(commandsNode);
            return xmlDocument;
        }

        public XmlDocument GetHelpErrorDocument(string errorMessage)
        {
            var xmlDocument = new XmlDocument();

            var rootNode = xmlDocument.CreateElement("Error");

            rootNode.InnerText = errorMessage;

            xmlDocument.AppendChild(rootNode);

            return xmlDocument;
        }
    }
}

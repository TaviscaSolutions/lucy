﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace Help
{
    public class HelpManager
    {
        private readonly HelpXmlManager _helpXmlManager;

        public HelpManager()
        {
            _helpXmlManager = new HelpXmlManager();
        }

        public XmlDocument Help(Dictionary<string,string> commanDictionary, string name = null)
        {
            var helpDocument = _helpXmlManager.GetDefaultXmlDocument();

            if (commanDictionary == null)
            {
                return _helpXmlManager.GetHelpErrorDocument("Oops commands not available");
            }

            if (name == null)
            {
                foreach (var argument in commanDictionary)
                {
                    _helpXmlManager.AddCommandToXmlDocument(helpDocument, argument.Key, argument.Value.Split(',').ToList());
                }

                return helpDocument;
            }

            if (commanDictionary.ContainsKey(name))
            {   
                _helpXmlManager.AddCommandToXmlDocument(helpDocument, name, commanDictionary[name].Split(',').ToList());
                
                return helpDocument;
            }

            return _helpXmlManager.GetHelpErrorDocument("Oops error in command syntax or command not defined");
        }
    }
}

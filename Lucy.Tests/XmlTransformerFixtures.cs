﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Xml.XPath;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lucy.Core.Contracts;
using Lucy.Core;
using System.Xml;


namespace Lucy.Tests
{
    /// <summary>
    /// Summary description for XmlTransformerFixtures
    /// </summary>
    [TestClass]
    public class XmlTransformerFixtures
    {
        [TestMethod]
        public void ReturnTransformedHtmlStringForValidXmlInput()
        {
            XPathDocument xmlDocument = new XPathDocument(@"E:\lucy\Lucy.Core\FormatterFiles\xml\help.xml");

            var transformer = new XmlToHtmlTransformer();
            string result = transformer.Transform(xmlDocument);
            string checkResult = "<html><body><h2>Help</h2><table border=\"1\"><tr bgcolor=\"#527BBD\"><th style=\"text-align:left\">Type</th><th style=\"text-align:left\">Synopsis</th></tr><tr><td>Type1</td><td /></tr></table></body></html>";

            Assert.AreEqual(checkResult,result);
        }

        [TestMethod]
        public void ThrowExceptionOnInvalidXmlInput()
        {
            try
            {
                    XPathDocument xmlDocument = new XPathDocument(@"E:\lucy\Lucy.Core\FormatterFiles\xml\help.xml");
                    var transformer = new XmlToHtmlTransformer();
                    string result = transformer.Transform(xmlDocument);
                    string checkResult ="<html><body><h2>Help</h2><table border=\"1\"><col width=\"100\" /><col width=\"250\" /><tr bgcolor=\"#9acd32\"><th style=\"text-align:left\">Command</th><th style=\"text-align:left\">Usage</th></tr><tr><td> pull </td><td> git pull origin </td></tr><tr><td> push </td><td> git push origin master</td></tr></table></body></html>";
            }
            catch (XmlException ex)
            {
                Assert.AreEqual(typeof(XmlException),ex.GetType()); 
            }
        }

        [TestMethod]
        public void CreateObjectCorrespondingToAppropriateXmlInput()
        {

        }
    }
}

﻿using System.Collections.Generic;
using System.Xml;
using Help;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lucy.Tests.HelpFixture
{
    [TestClass]
    public class HelpXmlManagerFixture
    {
        private HelpXmlManager _helpXmlManager;

        [TestInitialize]
        public void BeforeTest()
        {
            _helpXmlManager = new HelpXmlManager();
        }

        [TestCleanup]
        public void AfterTest()
        {
            _helpXmlManager = null;
        }

        [TestMethod]
        public void TryGetDefaultXmlDocument()
        {
            var xmlDocument = _helpXmlManager.GetDefaultXmlDocument();

            var commandNodes = xmlDocument.SelectNodes("//Commands");

            Assert.IsNotNull(xmlDocument);
            Assert.AreEqual(1, commandNodes.Count);
        }

        [TestMethod]
        public void TryAddCommandToXmlDocumentWithNullDocument()
        {
            XmlDocument xmlDocument = null;
            
            _helpXmlManager.AddCommandToXmlDocument(xmlDocument, null, null);

            Assert.IsNull(xmlDocument);
        }

        [TestMethod]
        public void TryAddCommandToXmlDocumentWithNullCommandsElement()
        {
            var xmlDocument = new XmlDocument();

            _helpXmlManager.AddCommandToXmlDocument(xmlDocument, null, null);
            
            var commandNodes = xmlDocument.SelectNodes("//Commands");
            
            Assert.IsNotNull(xmlDocument);
            Assert.AreEqual(0,commandNodes.Count);
        }
        
        [TestMethod]
        public void AddCommandToXmlDocument()
        {
            var xmlDocument = _helpXmlManager.GetDefaultXmlDocument();
            var commandName = "fetch_employee_details";
            var commandParameteres = new List<string> { "2", "10000" };
            _helpXmlManager.AddCommandToXmlDocument(xmlDocument, commandName, commandParameteres);

            var commandElements = xmlDocument.SelectNodes("//Commands/Command");
            var commandParameterElements = xmlDocument.SelectNodes("//Commands/Command/CommandArgument");

            Assert.IsNotNull(xmlDocument);
            Assert.IsNotNull(commandElements);
            Assert.IsNotNull(commandParameterElements);

            Assert.AreEqual(1, commandElements.Count);
            Assert.AreEqual(2, commandParameterElements.Count);
        }
    }
}

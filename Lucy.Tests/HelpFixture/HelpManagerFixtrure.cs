﻿using System;
using System.Collections.Generic;
using Help;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lucy.Tests.HelpFixture
{
    [TestClass]
    public class HelpManagerFixtrure
    {
        private HelpManager _helpManager;
        private Dictionary<String, String> _commandDictionary;

        [TestInitialize]
        public void BeforeTest()
        {
            _helpManager = new HelpManager();
            _commandDictionary = new Dictionary<string, string>();
            _commandDictionary["get_employee_details"] = "emp_name";
            _commandDictionary["get_employee_by_age_name"] = "emp_name,emp_age";
        }


        [TestCleanup]
        public void AfterTest()
        {
            _helpManager = null;
            _commandDictionary = null;
        }

        [TestMethod]
        public void TryHelpForCommandsNotDeclared()
        {
            var helpDocument = _helpManager.Help(null);

            Assert.IsNotNull(helpDocument);

            var errorElement = helpDocument.SelectNodes("//Error");

            Assert.IsNotNull(errorElement);

            Assert.AreEqual(1, errorElement.Count);
        }

        [TestMethod]
        public void TryHelpForCommandsNameNotDeclared()
        {
            var helpDocument = _helpManager.Help(_commandDictionary,"im_not_defined");

            Assert.IsNotNull(helpDocument);

            var errorElement = helpDocument.SelectNodes("//Error");

            Assert.IsNotNull(errorElement);

            Assert.AreEqual(1, errorElement.Count);
        }

        [TestMethod]
        public void TryHelpForAllCommands()
        {
            var helpDocument =_helpManager.Help(_commandDictionary);

            Assert.IsNotNull(helpDocument);

            var commandElements = helpDocument.SelectNodes("//Commands/Command");
            var commandParameterElements = helpDocument.SelectNodes("//Commands/Command/CommandArgument");
            
            Assert.IsNotNull(commandElements);
            Assert.IsNotNull(commandParameterElements);

            Assert.AreEqual(2, commandElements.Count);
            Assert.AreEqual(3, commandParameterElements.Count);

        }

        [TestMethod]
        public void TryHelpForSingleCommands()
        {
            var helpDocument = _helpManager.Help(_commandDictionary, "get_employee_by_age_name");

            Assert.IsNotNull(helpDocument);

            var commandElements = helpDocument.SelectNodes("//Commands/Command");
            var commandParameterElements = helpDocument.SelectNodes("//Commands/Command/CommandArgument");

            Assert.IsNotNull(commandElements);
            Assert.IsNotNull(commandParameterElements);

            Assert.AreEqual(1, commandElements.Count);
            Assert.AreEqual(2, commandParameterElements.Count);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Lucy.Core.Contracts;
using Lucy.Core;
using Lucy.Core.Model;
using Lucy.Core.CustomExceptions;

namespace Lucy.Tests
{
    [TestClass]
    public class ChannelFixtures
    {
        private Dictionary<string, HashSet<string>> _subscriptions = new Dictionary<string, HashSet<string>>();
        private Dictionary<string, ICanRecieveMessage> _subscibers = new Dictionary<string, ICanRecieveMessage>();

        [TestMethod]
        public void NullObjectReceivedShouldThrowException()
        {
            Mock<IChannel> mockIChannel = new Mock<IChannel>();
            string topic = null;
            Mock<ICanRecieveMessage> mockICanReceiveMessage = new Mock<ICanRecieveMessage>();


            mockIChannel.Setup(x => x.Subscribe(It.IsAny<string>(), It.IsAny<ICanRecieveMessage>()))
                                .Callback((string topic1, ICanRecieveMessage handler) =>
                                {
                                    if (handler == null)
                                        throw new ArgumentNullException("handler");
                                });
            try
            {
                mockIChannel.Object.Subscribe(topic, mockICanReceiveMessage.Object);
            }
            catch (ArgumentNullException e)
            {
                Assert.AreEqual(e.ParamName, "handler");
            }
        }

        [TestMethod]
        public void NullTopicShouldThrowNullTopicException()
        {
            Mock<IChannel> mockIChannel = new Mock<IChannel>();
            string topic = null;
            Mock<ICanRecieveMessage> mockICanReceiveMessage = new Mock<ICanRecieveMessage>();


            mockIChannel.Setup(x => x.Subscribe(It.IsAny<string>(), It.IsAny<ICanRecieveMessage>()))
                                .Callback((string topic1, ICanRecieveMessage handler) =>
                                {
                                    if (topic1 == null)
                                        throw new ArgumentNullException("topic");
                                });
            try
            {
                mockIChannel.Object.Subscribe(topic, mockICanReceiveMessage.Object);
            }
            catch (ArgumentNullException e)
            {
                Assert.AreEqual(e.ParamName, "topic");
            }
        }

        [TestMethod]
        public void EmptyTopicShouldThrowEmptyTopicException()
        {
            Mock<IChannel> mockIChannel = new Mock<IChannel>();
            string topic = string.Empty;
            Mock<ICanRecieveMessage> mockICanReceiveMessage = new Mock<ICanRecieveMessage>();
            mockIChannel.Setup(x => x.Subscribe(It.IsAny<string>(), It.IsAny<ICanRecieveMessage>()))
                               .Callback((string topic1, ICanRecieveMessage msg) =>
                               {
                                   if (topic1 == string.Empty)
                                       throw new ArgumentException("Empty topic");
                               });

            try
            {
                mockIChannel.Object.Subscribe(topic, mockICanReceiveMessage.Object);
            }
            catch (ArgumentException e)
            {
                Assert.AreEqual(e.Message, "Empty topic");
            }
        }

        [TestMethod]
        public void TopicNotSubscribedShouldThrowTopicNotPresentException()
        {
            Mock<IChannel> mockIChannel = new Mock<IChannel>();
            Mock<ICanRecieveMessage> mockICanReceiveMessage = new Mock<ICanRecieveMessage>();
            string topic = "SQL";
            string payload = "tables";

            mockIChannel.Setup(x => x.Subscribe(It.IsAny<string>(), It.IsAny<ICanRecieveMessage>()))
                              .Callback((string topic1, ICanRecieveMessage sql) =>
                              {
                                  _subscibers.Add(sql.Id, sql);
                                  HashSet<string> ids = new HashSet<string>();
                                  ids.Add(sql.Id);
                                  _subscriptions.Add(topic1, ids);
                              });
            mockIChannel.Setup(x => x.Send(It.IsAny<Message>()))
                         .Callback((Message topicMsg) =>
                         {
                             if (topicMsg is TopicMessage)
                             {
                                 TopicMessage tmsg = topicMsg as TopicMessage;
                                 if (_subscriptions.ContainsKey(tmsg.Topic) == false)
                                     throw new ChannelCustomException(1, "Topic not present exception");
                             }
                         });
            try
            {

                mockICanReceiveMessage.Setup(x => x.Id).Returns("Sql");

                mockIChannel.Object.Subscribe(topic, mockICanReceiveMessage.Object);
                TopicMessage topicMessage = new TopicMessage(mockICanReceiveMessage.Object, "MySQL", payload);
                mockIChannel.Object.Send(topicMessage);
            }
            catch (ChannelCustomException e)
            {
                Assert.AreEqual(e.Message, "Topic not present exception");
            }

        }

        [TestMethod]
        public void ValidTopicMessageShouldCallAppropriateSubscriber()
        {
            Mock<IChannel> mockIChannel = new Mock<IChannel>();
            Mock<ICanRecieveMessage> mockICanReceiveMessage = new Mock<ICanRecieveMessage>();
            string topic = "SQL";
            string payload = "tables";

            mockIChannel.Setup(x => x.Subscribe(It.IsAny<string>(), It.IsAny<ICanRecieveMessage>()))
                              .Callback((string topic1, ICanRecieveMessage sql) =>
                              {
                                  _subscibers.Add(sql.Id, sql);
                                  HashSet<string> ids = new HashSet<string>();
                                  ids.Add(sql.Id);
                                  _subscriptions.Add(topic1, ids);
                              });
            mockIChannel.Setup(x => x.Send(It.IsAny<Message>()))
                         .Callback((Message topicMsg) =>
                         {
                             if (topicMsg is TopicMessage)
                             {
                                 TopicMessage tmsg = topicMsg as TopicMessage;
                                 ICanRecieveMessage subscriber;
                                 if (_subscriptions.ContainsKey(tmsg.Topic) == true)
                                 {
                                     _subscibers.TryGetValue("Sql", out subscriber);
                                     subscriber.Notify(tmsg);
                                 }
                             }
                         }).Verifiable();

            mockICanReceiveMessage.Setup(x => x.Id).Returns("Sql");

            mockIChannel.Object.Subscribe(topic, mockICanReceiveMessage.Object);
            TopicMessage topicMessage = new TopicMessage(mockICanReceiveMessage.Object, "SQL", payload);
            mockIChannel.Object.Send(topicMessage);
            mockICanReceiveMessage.Verify(mock => mock.Notify(topicMessage), Times.Once());
        }

        [TestMethod]
        public void SubscriberShoudBeAddedSuccessfully()
        {
            Mock<IChannel> mockIChannel = new Mock<IChannel>();
            Mock<ICanRecieveMessage> mockICanReceiveMessage = new Mock<ICanRecieveMessage>();
            string topic = "SQL";

            mockIChannel.Setup(x => x.Subscribe(It.IsAny<string>(), It.IsAny<ICanRecieveMessage>()))
                                .Callback((string topic1, ICanRecieveMessage sql) =>
                                {
                                    _subscibers.Add(sql.Id, sql);
                                    HashSet<string> ids = new HashSet<string>();
                                    ids.Add(sql.Id);
                                    _subscriptions.Add(topic1, ids);
                                });



            mockICanReceiveMessage.Setup(x => x.Id).Returns("Sql");
            mockIChannel.Object.Subscribe(topic, mockICanReceiveMessage.Object);
            Assert.IsTrue(_subscriptions.ContainsKey("SQL"));
            HashSet<String> id;
            _subscriptions.TryGetValue("SQL", out id);
            Assert.IsTrue(id.Contains("Sql"));

            ICanRecieveMessage subscriber;
            Assert.IsTrue(_subscibers.ContainsKey("Sql"));
            _subscibers.TryGetValue("Sql", out subscriber);
            Assert.IsTrue(subscriber != null);
        }
    }
}

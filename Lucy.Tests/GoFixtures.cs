﻿using GoIntegration;
using Lucy.Core.CustomAttributes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace Lucy.Tests
{
    [TestClass]
    public class GoFixtures
    {
        //[TestMethod]
        //public void GoSubscriberShouldListenToChannel()
        //{
        //    throw new NotImplementedException();
        //}
        //[TestMethod]
        //public void TopicInMessageObjectProvidedByDispatcherShouldNotBeNull()
        //{
        //    throw new NotImplementedException();
        //}
        //[TestMethod]
        //public void PayloadInMessageObjectProvidedByDispatcherShouldNotBeNull()
        //{
        //    throw new NotImplementedException();
        //}
        //[TestMethod]
        //public void TypeInCommandObjectProvidedByDispatcherShouldNotBeNull()
        //{
        //    throw new NotImplementedException();
        //}
        //[TestMethod]
        //public void ArgumentsInCommandObjectProvidedByDispatcherShouldNotBeNull()
        //{
        //    throw new NotImplementedException();
        //}
        //[TestMethod]
        //public void TriggerPipelineInstanceWithCommitTagShouldRunThePipelineWithThatCommitTag()
        //{
        //    throw new NotImplementedException();
        //}
        
        [TestMethod]
        public void TriggerPipelineInstanceShouldRunThePipeline()
        {
            string pipeline = "hfl-local";
            GoIntegrator goIntegrator=new GoIntegrator();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc=goIntegrator.TriggerPipelineInstance(pipeline);
            Assert.IsNotNull(xmlDoc, "TEXT" + xmlDoc);
        }
        [TestMethod]
        public void UnavailabilityOfGoServerShouldThrowServerNotFoundException()
        {
            string pipeline = "hfl-localzxy";
            GoIntegrator goIntegrator = new GoIntegrator();
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc = goIntegrator.TriggerPipelineInstance(pipeline);
            }
            catch(GoCustomExceptions ex)
            {
                Assert.AreEqual(ex.Type, "GoServerNotFound");
           }
                
        }
        [TestMethod]
        public void GetCommandAttributesShouldReturnCommandAttributes()
        {
            string name = "TriggerPipelineInstance";
            MethodAttributeReader attributesRetriever = new MethodAttributeReader();
            CommandAttribute commandAttribute = new CommandAttribute();
            commandAttribute = attributesRetriever.GetCommandAttributes(name, typeof(GoIntegrator));
            Assert.IsNotNull(commandAttribute);
        }
    }
}

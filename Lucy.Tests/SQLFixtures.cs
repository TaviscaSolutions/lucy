﻿using System;
using System.Data;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using Lucy.Handlers.MySql;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lucy;
using Lucy.Core.Model;
using SQLIntegration.SQLException;
using SQLIntegration;
using Lucy.Handlers.SQLIntegration;
using Moq;
using System.Data.SqlClient;
using IDatabase = Lucy.Handlers.SQLIntegration.IDatabase;
using TableAdapter = Lucy.Handlers.SQLIntegration.TableAdapter;

namespace Lucy.Tests
{
    [TestClass]
    public class SQLFixtures
    {
        Command _command;                
        ISQLCommandHandler _sqlHandler;
        IDatabase _sqlDatabaseObj;
        TableAdapter _xmlConvertor;

        [TestInitialize]
        public void init()
        {
            _command = new Command();            
            _sqlHandler = new SQLCommandHandler();
            _xmlConvertor = new TableAdapter();
            _sqlDatabaseObj = new SqlDatabase();
        }     

        [TestMethod]
        public void CheckIfCommandTypeIsValid()
        {
            _command.Type = "SQL";
            _command.Arguments["query_name"] = "select * from Gamer";
            
            _sqlHandler.Execute(_command);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCommandTypeException))]
        public void CheckIfCommandTypeIsInValid()
        {
            _command.Type = "";
            _sqlHandler.Execute(_command);
        }

        [TestMethod]
        public void CheckIfQueryNameInCommandArgumentsIsEmpty()
        {
            try
            {
                _command.Type = "SQL";
                _command.Arguments["sp_name"] = "";
                _sqlHandler.Execute(_command);
            }
            catch (ArgumentNullException e)
            {
            }
        }

        [TestMethod]
        public void CheckIfQueryNameInCommandArgumentsIsValid()
        {
            _command.Type = "SQL";
            _command.Arguments["sp_name"] = "Select_game";
            _command.Arguments["Game_id"] = "1";
            _sqlHandler.Execute(_command);            
        }

        [TestMethod]        
        public void DatabaseConnectionTestWhenConnectionStringIsEmpty()
        {
            try
            {
                _sqlDatabaseObj = new SqlDatabase();
            }
            catch (DatabaseConnectionException e)
            {
                Assert.IsTrue(e.Message.Contains("empty"));
            }
        }

        [TestMethod]
        public void DatabaseConnectionTestWhenConnectionStringIsNull()
        {
            try
            {
                _sqlDatabaseObj = new SqlDatabase();

            }
            catch (DatabaseConnectionException e)
            {
                Assert.IsTrue(e.Message.Contains("null"));
            }
        }
        
        [TestMethod]
        public void ValidResultTest()
        {
            SqlCommand comm = new SqlCommand();
            Dictionary<string, object> argumentDictionary = new Dictionary<string, object>();
            DataTable table = new DataTable();
            var databaseMock = new Mock<IDatabase>();
            databaseMock.Setup(x => x.GetResult(comm, argumentDictionary)).Returns(table);
            XmlDocument xmlResult = _xmlConvertor.ConvertToXml(table);
        }

        [TestMethod]
        public void InvalidResultTest()
        {
            try
            {
                SqlCommand comm = new SqlCommand();
                Dictionary<string, object> argumentDictionary = new Dictionary<string, object>();
                DataTable table = null;
                var databaseMock = new Mock<IDatabase>();
                databaseMock.Setup(x => x.GetResult(comm, argumentDictionary)).Returns(table);
                XmlDocument xmlResult = _xmlConvertor.ConvertToXml(table);
            }
            catch (NullReferenceException e)
            {

            }
        }
        
        [TestMethod]
        public void ResultConversionTest()
        {
            DataTable table = new DataTable();
            XmlDocument xmlResult = _xmlConvertor.ConvertToXml(table);

        }

        [TestMethod]
        public void CheckCreationOfInputParameters()
        {
            SqlCommand comm = new SqlCommand();
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary["sp_name"] = "Select_game";
            dictionary["Game_id"] = 1;
            DataTable table = null;
            table = _sqlDatabaseObj.GetResult(comm, dictionary);
        }
    }
}

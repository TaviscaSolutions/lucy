﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySql;
using MySql.Data.MySqlClient;
using Lucy.Handlers.MySql;
using Lucy.Handlers.MySql.CustomException;
using System.Xml;
using System.Data;
using System.Data.Entity;
using Lucy.Core.Contracts;
using Lucy.Core.Model;
using System.Collections.Generic;

namespace Lucy.Tests
{
    [TestClass]
    public class MySqlIntegrationFixture
    {
        
        [TestMethod]
        [ExpectedException(typeof(InvalidConnectionStringException))]
        public void NullConnectionStringShouldReturnInvalidConnectionStringException()
        {
            string ConnectionString = null;
            var mySqlDatabase = new MySqlDatabase(ConnectionString);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConnectionStringException))]
        public void EmptyConnectionStringShouldReturnInvalidConnectionStringException()
        {
            string ConnectionString = string.Empty;
            var mySqlDatabase = new MySqlDatabase(ConnectionString);
        }

       
        [TestMethod]
        public void NullResultShouldConvertToEmptyXMLDocument()
        {
            DataTable table = new DataTable();
            var xmlConvertor = new TableAdapter();
            XmlDocument xmlDoc = xmlConvertor.ToXml(table);
            Assert.AreEqual(xmlDoc.DocumentType,null);
        }


        [TestMethod]
        public void ValidSQLCommandShouldReturnResult()
        {
            string ConnectionString = "Server=127.0.0.1;Port=3307;Database=world;Uid=root;Pwd=123456789;";
            string query = "Select * from city where CountryCode='AFG'";
            Command command = new Command();
            command.Type = "MySql";
            command.Arguments.Add("query_name", query);

            MySqlHandler mySqlHandler = new MySqlHandler(ConnectionString);
            XmlDocument xmlDocument = mySqlHandler.Execute(command);
        }

        [TestMethod]
        public void ExistingSPShouldReturnResult()
        {
            Command command = new Command();
            command.Type = "MySql";
            command.Arguments.Add("sp_name", "TestSP");
            command.Arguments.Add("Inid", "1");
           
            string ConnectionString = "Server=127.0.0.1;Port=3307;Database=world;Uid=root;Pwd=123456789;";
            MySqlHandler mySqlHandler = new MySqlHandler(ConnectionString);
            XmlDocument xmlDocument = mySqlHandler.Execute(command);
        }

        [TestMethod]
        public void SPNotFoundShouldThrowProcedureNotFoundException()
        {
            Command command = new Command();
            command.Type = "MySql";
            command.Arguments.Add("sp_name", "TestSPException");
            command.Arguments.Add("Inid", "1");

            string ConnectionString = "Server=127.0.0.1;Port=3307;Database=world;Uid=root;Pwd=123456789;";
            MySqlHandler mySqlHandler = new MySqlHandler(ConnectionString);
            XmlDocument xmlDocument = mySqlHandler.Execute(command);
        }

        [TestMethod]
        public void ExpectedAndActualParameterCountDoesNotMatchShouldThrowInvalidParameterException()
        {
            Command command = new Command();
            command.Type = "MySql";
            command.Arguments.Add("sp_name", "TestInvalidArgument");
            command.Arguments.Add("Inid", "1");
            command.Arguments.Add("Test", "2");

            string ConnectionString = "Server=127.0.0.1;Port=3307;Database=world;Uid=root;Pwd=123456789;";
            MySqlHandler mySqlHandler = new MySqlHandler(ConnectionString);
            XmlDocument xmlDocument = mySqlHandler.Execute(command);
        }

        [TestMethod]
        public void ExpectedAndActualParameterNameDoesNotMatchShouldThrowInvalidParameterException()
        {
            Command command = new Command();
            command.Type = "MySql";
            command.Arguments.Add("sp_name", "TestInvalidArgument");
            command.Arguments.Add("Inid", "1");
            command.Arguments.Add("Test", "2");

            string ConnectionString = "Server=127.0.0.1;Port=3307;Database=world;Uid=root;Pwd=123456789;";
            MySqlHandler mySqlHandler = new MySqlHandler(ConnectionString);
            XmlDocument xmlDocument = mySqlHandler.Execute(command);
        }
    }
}

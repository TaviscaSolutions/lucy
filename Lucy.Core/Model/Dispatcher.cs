using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lucy.Core.Contracts;

namespace Lucy.Core.Model
{
    public class Dispatcher : IChannel
    {
        //Dictionary _subscriptions --topic,hashset of ids
        //Dictionary _subscribers--id,object of ICanReceiveMessage
        private Dictionary<string, HashSet<string>> _subscriptions = new Dictionary<string, HashSet<string>>();
        private Dictionary<string, ICanRecieveMessage> _subscibers = new Dictionary<string, ICanRecieveMessage>();

        public void Subscribe(string topic, ICanRecieveMessage subscriber)
        {
            ICanRecieveMessage handler;
            if (_subscibers.TryGetValue(subscriber.Id, out handler) == true)
                _subscibers[subscriber.Id] = subscriber;
            else
                _subscibers.Add(subscriber.Id, subscriber);
            HashSet<string> subscribers;
            if (_subscriptions.TryGetValue(topic, out subscribers) == false)
            {
                subscribers = new HashSet<string>();
                subscribers.Add(subscriber.Id);
                _subscriptions.Add(topic, subscribers);
            }
            else
            {
                if (subscribers.Contains(subscriber.Id) == false)
                {
                    subscribers.Add(subscriber.Id);
                    _subscriptions[topic] = subscribers;
                }
            }

        }

        public void UnSubscribe(string topic, ICanRecieveMessage subscriber)
        {
            HashSet<string> subscribers;
            if (_subscriptions.TryGetValue(topic, out subscribers) == false)
                return;
            if (subscribers.Contains(subscriber.Id) == true)
            {
                _subscibers.Remove(subscriber.Id);
                if (subscribers.Count == 1)
                    _subscriptions.Remove(topic);
                else
                {
                    subscribers.Remove(subscriber.Id);
                    _subscriptions[topic] = subscribers;
                }
            }
        }

        public void Send(Message message)
        {
            if (message is TopicMessage)
                RouteTopicMessage(message as TopicMessage);
            else
                RoutePointToPointMessage(message as PointToPointMessage);
        }

        private void RouteTopicMessage(TopicMessage message)
        {
            HashSet<string> subscribers;
            if (_subscriptions.TryGetValue(message.Topic, out subscribers) == false)
                return;
            foreach (var handler in subscribers)
            {
                ICanRecieveMessage subscriber = null;
                if (_subscibers.TryGetValue(handler, out subscriber) == true)
                    NotifyAsync(subscriber, message);
            }
        }

        private void RoutePointToPointMessage(PointToPointMessage message)
        {
            for (int i = 0; i < message.Recipients.Length; i++)
            {
                ICanRecieveMessage subscriber = null;
                if (_subscibers.TryGetValue(message.Recipients[i], out subscriber) == true)
                    NotifyAsync(subscriber, message);
            }
        }

        private void NotifyAsync(ICanRecieveMessage subscriber, Message message)
        {
            Action run = () =>
            {
                try
                {
                    subscriber.Notify(message);
                }
                catch { }
            };

            run.BeginInvoke(ar => { }, null);
        }
    }
}

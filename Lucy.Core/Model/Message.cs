﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lucy.Core.Contracts;
namespace Lucy.Core.Model
{
    public class Message
    { 
        public Message(ICanRecieveMessage sender, Object payload)
        {
            this.Sender = sender;
            this.Payload = payload;
            this.UtcTimestamp = DateTime.UtcNow;
        }

        public ICanRecieveMessage Sender { get; private set; }

        public object Payload { get; private set; }

        public DateTime UtcTimestamp { get; private set; }

        public void Reply(ICanRecieveMessage sender,object payload)
        {
            PointToPointMessage replyMessage= new PointToPointMessage(sender, payload, new string[] { this.Sender.Id });
            sender.Notify(replyMessage);
        }
    }

    public class TopicMessage : Message
    {
        public TopicMessage(ICanRecieveMessage sender, string topic, object payload)
            : base(sender, payload)
        {
            this.Topic = topic;
        }

        public string Topic { get; set; }
    }


    public class PointToPointMessage : Message
    {
        public PointToPointMessage(ICanRecieveMessage sender, object payload, string[] recipients)
            : base(sender, payload)
        {
            this.Recipients = recipients;
        }

        public string[] Recipients { get; private set; }
    }
   
}

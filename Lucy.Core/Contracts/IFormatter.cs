﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.XPath;

namespace Lucy.Core.Contracts
{
    public interface IFormatter
    {
        string Transform(XPathDocument xdocument);
    }
}

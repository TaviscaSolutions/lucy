﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lucy.Core.Model;
namespace Lucy.Core.Contracts
{
    public interface ICanRecieveMessage
    {
        string Id { get; set; }

        void Notify( Message message);
    }
}

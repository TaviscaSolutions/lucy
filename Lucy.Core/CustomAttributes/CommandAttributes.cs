﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lucy.Core.CustomAttributes
{
    public class CommandAttribute : Attribute
    {
        public string Type { get; set; }
        public string Desription { get; set; }
        public CommandAttribute()
        {

        }
        public CommandAttribute(string type, string description)
        {
            this.Type = type;
            this.Desription = description;
        }
    }
}

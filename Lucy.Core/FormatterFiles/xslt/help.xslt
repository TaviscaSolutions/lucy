<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- Edited by XMLSpy® -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h2>Help</h2>
            <table border="1">
              <tr bgcolor="#527BBD">
                <th style="text-align:left">Type</th>
                <th style="text-align:left">Synopsis</th>
              </tr>
              <xsl:for-each select="Help/Commands/Command">
              <tr>
                <td><xsl:value-of select="@Name"/></td>
                <td><xsl:value-of select="@Synopsis"/></td>
              </tr>
              </xsl:for-each>
            </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>


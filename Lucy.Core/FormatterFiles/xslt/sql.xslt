<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1">
	<xsl:output indent='yes' />

	<xsl:template match="DataSet/diffgr:diffgram/NewDataSet|DataSet/diffgr:diffgram/NewDataSet//*">
		<xsl:copy>		

			      <html>
				  <body>
				  <h2>DataSet</h2>
				    <table border="1">
				      <tr bgcolor="#9acd32">
				        <th style="text-align:left">roomCode</th>
				        <th style="text-align:left">roomNo</th>
				        <th style="text-align:left">hotelCode</th>
				      </tr>
				      <xsl:for-each select="Table1">
				      <tr>
				        <td><xsl:value-of select="roomCode"/></td>
				        <td><xsl:value-of select="roomNo"/></td>
				        <td><xsl:value-of select="hotelCode"/></td>
				      </tr>
				      </xsl:for-each>
				    </table>
				  </body>
				  </html>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
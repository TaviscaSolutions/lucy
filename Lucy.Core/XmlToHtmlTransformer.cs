﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lucy.Core.Contracts;
using System.Xml.XPath;
using System.Xml;
using System.Xml.Xsl;
using System.IO;
using System.Diagnostics;


namespace Lucy.Core
{
    public class XmlToHtmlTransformer:IFormatter
    {
        public string Transform(XPathDocument myXPathDoc)
        {
            String htmlFileContent = string.Empty;
            
            XmlTextWriter myWriter = new XmlTextWriter("result.html", null);


            XslCompiledTransform myXslTrans = new XslCompiledTransform();

            if (GetXmlType(myXPathDoc).Equals("Help"))
            {
                myXslTrans.Load(@"E:\lucy\Lucy.Core\FormatterFiles\xslt\help.xslt");
            }
            else if (GetXmlType(myXPathDoc).Equals("DataSet"))
            {
                myXslTrans.Load(@"E:\lucy\Lucy.Core\FormatterFiles\xslt\sql.xslt");
            }
            
            try
            {
                myXslTrans.Transform(myXPathDoc, null, myWriter);
            }
            catch (XmlException ex)
            {
                //create and throw new custom invalidargumentexception
                throw new XmlException("Invalid XML File");
            }

            myWriter.Close();
            htmlFileContent = File.ReadAllText("result.html");

            return htmlFileContent;
        }

        private string GetXmlType(XPathDocument xPathDoc)
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(xPathDoc.CreateNavigator().InnerXml);

            return xdoc.DocumentElement.Name;
        }
    }
}

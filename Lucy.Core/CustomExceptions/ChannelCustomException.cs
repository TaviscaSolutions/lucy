﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lucy.Core.CustomExceptions
{
    [Serializable]
    public class ChannelCustomException:Exception
    {
        public int ErrorCode { get; private set; }

        public string Message { get; private set; }

        public ChannelCustomException(int errorCode,string message): base() {
            this.ErrorCode = errorCode;
            this.Message = message;
        }
    }
}
